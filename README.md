# emoji keyboard
An emoji keyboard for the linux desktop.

## Technology
This is a layout for [matchbox-keyboard](https://github.com/xlab/matchbox-keyboard).

## Contribution
To add new emojis:

1. Get an emoji character from a website like [this one](http://www.iemoji.com/emoji-cheat-sheet/all)
1. Make sure it shows on facebook when using a desktop OS.
1. Create a 39X39 PNG image that shows your emoji and give it a meaningful name and place it in `~/.matchbox/`
1. Add an entry for the new emoji in the keyboard.xml

##TODO
- [ ] Make the keyboard always on top
- [ ] Package the keyboard for known linux distros (fedora, ubuntu, arch... etc.)
- [ ] Hack on the xml parser in the matchbox-keyboard to make it accept more than one character for an action attribute
- [ ] Add more emojis
- [ ] Make a "most recently used emojis" tab.
